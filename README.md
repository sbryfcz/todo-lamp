A simple LAMP based ToDo application

## Installation
* Install XAMP stack and make sure its working. 
* This uses PHP 7.1 (its not gaurenteed to run on other version)
* I used Homebrew to install 
``` brew install mysql ```


We've installed your MySQL database without a root password. To secure it run:
    mysql_secure_installation

To connect run:
    mysql -uroot

To have launchd start mysql now and restart at login:
  brew services start mysql
Or, if you don't want/need a background service you can just run:
  mysql.server start

brew services start mysql