<?php
    $servername = "localhost";
    $username = "root";
    $dbname = "todo";

    // Create connection
    $conn = new mysqli($servername, $username, NULL, $dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } 

    $note = mysqli_real_escape_string($conn, $_POST["note"]);

    $sql = "INSERT INTO todo_item (Note) VALUES ('".$note."');";
    $result = $conn->query($sql);

    if (!$result) {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
    else {
        echo "Entered data successfully\n";
    }

    $conn->close();

    header( 'Location: /todo.php' ) ;

?>