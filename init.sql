create database todo;
use todo;
create table todo_item
(
    ID int NOT NULL AUTO_INCREMENT,
    Note varchar(255) NOT NULL,
    PRIMARY KEY (ID)
);

-- Add a couple rows
insert into todo_item (Note)
VALUES ('Hello World');