<?php
    $servername = "localhost";
    $username = "root";
    $dbname = "todo";

    // Create connection
    $conn = new mysqli($servername, $username, NULL, $dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } 

    $id = mysqli_real_escape_string($conn, $_POST["id"]);

    $sql = "DELETE FROM todo_item WHERE ID=".$id.";";
    $result = $conn->query($sql);

    if (!$result) {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
    else {
        echo "Entered deleted successfully\n";
    }

    $conn->close();

    header( 'Location: /todo.php' ) ;

?>