<html>
 <head>
  <title>PHP Test</title>
 </head>
 <body>
    <h1>
        My ToDo Application
    </h1>
    <ul>
        <?php
            $servername = "localhost";
            $username = "root";
            $dbname = "todo";

            // Create connection
            $conn = new mysqli($servername, $username, NULL, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            } 

            $sql = "SELECT ID, Note FROM todo_item";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    echo '
                    <li>
                        <form action="remove_item.php" method="post">
                            <span>'.$row["Note"].'</span>
                            <input type="hidden" name="id" value="'.$row["ID"].'">
                            <input type="submit" value="Remove">
                        </form>
                    </li>
                    ';
                }
            }
            
            $conn->close();
        ?>
    </ul>
    <form action="add_item.php" method="post">
        New Item: 
        <input type="text" name="note">
        <input type="submit" value="Add Item">
    </form>
 </body>
</html>